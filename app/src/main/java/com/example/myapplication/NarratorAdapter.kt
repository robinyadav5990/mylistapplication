package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.Sort
import com.example.myapplication.model.narratorlist

class NarratorAdapter(
    val narrator_list: MutableList<narratorlist>
) : RecyclerView.Adapter<NarratorAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val tv_narratot = itemView.findViewById<TextView>(R.id.tv_narrator)
        val tv_amount = itemView.findViewById<TextView>(R.id.tv_amount)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NarratorAdapter.MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.lisitem_narrator,parent,false)
        return NarratorAdapter.MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return narrator_list.size
    }

    override fun onBindViewHolder(holder: NarratorAdapter.MyViewHolder, position: Int) {
        holder.tv_narratot.text = narrator_list.get(position).narration.toString()
        holder.tv_amount.text= narrator_list.get(position).amount.toString()

    }
}

