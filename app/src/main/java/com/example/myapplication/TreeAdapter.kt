package com.example.myapplication

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.Sort
import com.example.myapplication.model.narratorlist

class TreeAdapter(
    val dataList: List<Sort>,
    val set: List<Int>,
    val activity: Activity
) : RecyclerView.Adapter<TreeAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val tv_mid = itemView.findViewById<TextView>(R.id.tv_mid)
        val leafRecyclerView = itemView.findViewById<RecyclerView>(R.id.leafRecycler)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TreeAdapter.MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.listitem_mid,parent,false)
        return TreeAdapter.MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return set.size
    }

    override fun onBindViewHolder(holder: TreeAdapter.MyViewHolder, position: Int) {
        holder.tv_mid.text = set.get(position).toString()
        holder.leafRecyclerView.visibility = View.GONE
        val manager = LinearLayoutManager(activity)
        holder.leafRecyclerView?.layoutManager = manager
//        var mid_list = mutableListOf<Int>()
        var tid_list= mutableListOf<Int>()
//        var narraterList = mutableListOf<narratorlist>()
        for(i in 0..dataList.size-1){
//            mid_list.add(dataList.get(i).Mid)
            if(set.get(position) == dataList.get(i).Mid ){
            tid_list.add(dataList.get(i).Tid)
//            narraterList.add(narratorlist(dataList.get(i).amount,dataList.get(i).narration))
            }
        }
        val set = tid_list.sorted().toMutableSet()
        holder.leafRecyclerView?.adapter = LeafAdapter(dataList,set.toList(),activity)
        holder.itemView.setOnClickListener{
            if(holder.leafRecyclerView.visibility == View.GONE){
                holder.leafRecyclerView.visibility = View.VISIBLE
            }
            else{
                holder.leafRecyclerView.visibility = View.GONE
            }
        }


    }
}
