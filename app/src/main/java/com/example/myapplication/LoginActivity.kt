package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import java.util.regex.Matcher
import java.util.regex.Pattern


class LoginActivity : AppCompatActivity() {
    lateinit var etUsername : EditText
    lateinit var etPassword : EditText
    lateinit var BtnSigin : Button
    lateinit var errorUsername :TextView
    var mEmail : String ? = null
    var mpassword : String ? = null
    val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+".toRegex()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        etUsername= findViewById(R.id.username)

        etPassword= findViewById(R.id.password)

        BtnSigin= findViewById(R.id.btn_signin)
        errorUsername = findViewById<TextView>(R.id.errorUsername)
        etUsername.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val isEmailPatternMatched = checkEmailPattern(s.toString())
                if (isEmailPatternMatched == true) {
                    errorUsername.visibility = View.GONE
                }
                else{
                    errorUsername.visibility = View.VISIBLE
                }
            }
        })

        BtnSigin.setOnClickListener{
            mEmail= etUsername.text.toString()
            mpassword = etPassword.text.toString()
            submit()
        }


    }

    private fun submit() {
        if (errorUsername.visibility  == View.VISIBLE || (mEmail!!.isEmpty()) || !(mEmail!!.trim { it <= ' ' }.matches(emailPattern))) {
            errorUsername.visibility =View.VISIBLE
        }
        else if(mpassword!!.length < 8) {
            errorUsername.visibility =View.GONE
        }
        else{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            this.finish();
        }
//         {
//             errorUsername.visibility =View.GONE
//            val queue = Volley.newRequestQueue(this)
//
//             val jsonObject2 = JSONObject()
//             jsonObject2.put("email", mEmail)
//             jsonObject2.put("password", mpassword)
//            // Request a string response from the provided URL.
//            val url = "http://52.5.173.237/login.php"
//             val request = JsonObjectRequest(
//                 Request.Method.POST,url,jsonObject2, { resp ->
//
//                     val jsonResponse = (resp)
//                     if (jsonResponse.getBoolean("status")) {
//                         if(jsonResponse.getString("message") =="User Authentication Successful !!"){
//                             val intent = Intent(this, MainActivity::class.java)
//                             startActivity(intent)
//                             this.finish();}
//                     }
//                     else{
//                         Toast.makeText(this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show()
//                     } }, { resp ->
//                     Toast.makeText(this, resp.message, Toast.LENGTH_SHORT).show()  }
//             )
//             queue.add(request)
//        }
    }

    fun checkEmailPattern(email : String) : Boolean?{
        val isEmailPatternMatched : Boolean?
        val pattern: Pattern = Pattern.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]{2,}+")
        val matcher: Matcher = pattern.matcher(email)
        isEmailPatternMatched = matcher.matches()
        return isEmailPatternMatched

    }
}