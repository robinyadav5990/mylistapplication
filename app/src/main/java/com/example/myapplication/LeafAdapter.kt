package com.example.myapplication

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.Sort
import com.example.myapplication.model.narratorlist

class LeafAdapter(
    val dataList: List<Sort>,
    val tid_list: List<Int>,
    val activity: Activity
) : RecyclerView.Adapter<LeafAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val tv_tid = itemView.findViewById<TextView>(R.id.tv_tid)
        val narratorRecycler = itemView.findViewById<RecyclerView>(R.id.narratorRecycler)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeafAdapter.MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.listitem_tid,parent,false)
        return LeafAdapter.MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tid_list.size
    }

    override fun onBindViewHolder(holder: LeafAdapter.MyViewHolder, position: Int) {
        holder.tv_tid.text = tid_list.get(position).toString()

        holder.narratorRecycler.visibility = View.GONE
        val manager = LinearLayoutManager(activity)
        holder.narratorRecycler?.layoutManager = manager
//        var mid_list = mutableListOf<Int>()
        var narraterList = mutableListOf<narratorlist>()
        for(i in 0..dataList.size-1){
//            mid_list.add(dataList.get(i).Mid)
            if(tid_list.get(position) == dataList.get(i).Tid ){
                narraterList.add(narratorlist(dataList.get(i).amount,dataList.get(i).narration))
            }
        }
        val set : MutableList<narratorlist> = narraterList.sortedBy {it.narration} as MutableList<narratorlist>
        holder.narratorRecycler?.adapter = NarratorAdapter(set)
        holder.itemView.setOnClickListener{
            if(holder.narratorRecycler.visibility == View.GONE){
                holder.narratorRecycler.visibility = View.VISIBLE
            }
            else{
                holder.narratorRecycler.visibility = View.GONE
            }
        }

    }
}
