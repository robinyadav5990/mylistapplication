package com.example.myapplication.model

data class Sort(
    val Mid: Int,
    val Tid: Int,
    val amount: Double,
    val narration: Long
)