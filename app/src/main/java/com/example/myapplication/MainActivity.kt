package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.jsonModel
import com.example.myapplication.model.narratorlist
import com.google.gson.Gson

class MainActivity : AppCompatActivity() {
    var jsonString : String ? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getDataFromJson()
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val manager = LinearLayoutManager(this)
        recyclerView?.layoutManager = manager
        val list = getDataFromJson().sort.sortedBy { it.Mid }
        var mid_list = mutableListOf<Int>()
        for(i in 0..list.size-1){
            mid_list.add(list.get(i).Mid)
        }
        val set = mid_list.toMutableSet()
        recyclerView?.adapter = TreeAdapter(list, set.toList(),this)

    }

    private fun getDataFromJson():jsonModel {
        jsonString = this.assets.open("sort.json").bufferedReader().use{
            it.readText()
        }
        val resp = Gson().fromJson(
            jsonString,
            jsonModel ::class.java
        )
        return resp
    }
}